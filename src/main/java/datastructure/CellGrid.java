package datastructure;

import cellular.CellState;


public class CellGrid implements IGrid {

    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {

        // Fill grid with cells
        grid = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                grid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return grid.length;
    }

    @Override
    public int numColumns() {
        return grid[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (!inBounds(row, column)) {
            throw new IndexOutOfBoundsException("Invalid cell. Out of range.");
        }
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if (!inBounds(row, column)) {
            throw new IndexOutOfBoundsException("Invalid cell. Out of range.");
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        return this;
    }

    @Override
    public boolean inBounds(int row, int col) {
        return row >= 0 && row < numRows() && col >= 0 && col < numColumns();
    }
}
